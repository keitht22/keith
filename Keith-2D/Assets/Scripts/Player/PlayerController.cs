﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb;
    public Animator m_Animator;
    public TextMeshProUGUI countText;
    public SpriteRenderer sprite;
    public GameObject relic;
    
    public int count;

    public void Start()
    {
        count = 0;

        SetCountText();
        m_Animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        relic = GetComponent<GameObject>();
    }

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool m_isWalking = hasHorizontalInput;

        m_Animator.SetBool("IsWalking", m_isWalking);

        rb.velocity = new Vector2(horizontal * 3f, rb.velocity.y);

        if (Input.GetKeyDown("space"))
        {
            rb.velocity = new Vector2(rb.velocity.x, 9.5f);
        }
    }
    private void OnTriggerEnter()
    {
        if (relic.CompareTag("PickUp"))
        {
            relic.SetActive(false);

            count = count + 1;

            SetCountText();
        }
    }
    private void SetCountText()
    {
        countText.text = "Relics: " + count.ToString();
    }
}